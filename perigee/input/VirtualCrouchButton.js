import VirtualBtn from './VirtualButton.js'

class VirtualCrouchBtnInput extends VirtualBtn {
  constructor(btnElement) {
    super(btnElement)
  }
}

export default VirtualCrouchBtnInput
